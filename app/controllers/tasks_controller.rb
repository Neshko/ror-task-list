class TasksController < ApplicationController
  before_filter :find_item, only: [:edit, :update, :destroy, :show] #перед цими діями буде викликаний find_item

  def new # GET
    @task = Task.new
  end

  def create #POST
    @task = Task.create item_params
    redirect_to action: 'index'
  end

  def item_params #GET or POST
    params.require(:task).permit(:id, :text, :actual)
  end

  def index #GET запрос
    @tasks = Task.all
  end

  def edit #GET
  end

  def update #PUT
    @task.update_attributes item_params
    if @task.errors.empty?
      redirect_to action: 'index'
    else
      render 'edit'
    end
  end

  def destroy
    @task.destroy
    redirect_to action: 'index'
  end

  def show
  end

  def search
    search_key = params[:name]
    if search_key
      @tasks = Task.where('actual Like ?', "%#{search_key}%")
    end
  end

  private
  def find_item
    @task = Task.where(id: params[:id]).first
  end
end

